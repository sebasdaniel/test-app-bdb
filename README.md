## App Locations (Spring Boot + MySQL)

### Prerequisites

To get running this project you need Java, Maven (or an IDE with the maven support) and configured docker-compose

### Run Steps

To run the project follow the next steps:
- Create and run the mysql docker with the command: `sudo docker-compose up` and must have th file in the directory you run the command. The docker-compose file is placed inside the project files in: *src/main/resources/docker-compose.yml*
- Execute SQL script with the command: `mysql -h 127.0.0.1 -u bdb -D location -p < src/main/resources/script.sql`, the script file is again in the project path and the user password can be found in the docker-compose.yml file
- Compile the Spring Boot app with the command (in the project path): `mvn clean install`. The jar is placed in target folder.
- Run the app with the command: `java -jar target/<app-name>.jar`