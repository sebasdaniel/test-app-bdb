package com.bdb.locations.controller;

import java.util.List;

import com.bdb.locations.model.Location;
import com.bdb.locations.service.contract.ILocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class LocationController {
    
    @Autowired
    private ILocationService serviceLocation;


    @GetMapping("/location")
    public List<Location> getAllLocations() {
        return serviceLocation.getAll();
    }


    @GetMapping("/location/{id}")
    public Location getLocation(@PathVariable("id") Integer idLocation) {
        return serviceLocation.getById(idLocation);
    }


    @PostMapping("/location")
    public Location create(Location location, BindingResult result) {

        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not Valid Location Object");
        }

        if (location.getId() != null) {
            location.setId(null);
        }

        location = serviceLocation.save(location);

        return location;
    }
    
}