package com.bdb.locations.service;

import java.util.List;
import java.util.Optional;

import com.bdb.locations.model.Location;
import com.bdb.locations.repository.LocationRepository;
import com.bdb.locations.service.contract.ILocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationServiceJpa implements ILocationService {

    @Autowired
    private LocationRepository repoLocation;

    @Override
    public Location save(Location location) {
        return repoLocation.saveAndFlush(location);
    }

    @Override
    public Location getById(Integer idLocation) {
        
        Optional<Location> optional = repoLocation.findById(idLocation);

        if (optional.isPresent()) {
            return optional.get();
        }

        return null;
    }

    @Override
    public List<Location> getAll() {
        return repoLocation.findAll();
    }

    @Override
    public List<Location> getInternalLocationsByLocation(Integer idLocation) {
        return repoLocation.getInternalLocationsByLocationId(idLocation);
    }
    
}