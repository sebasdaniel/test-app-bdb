package com.bdb.locations.service.contract;

import java.util.List;

import com.bdb.locations.model.Location;

public interface ILocationService {
    
    Location save(Location location);

    Location getById(Integer idLocation);

    List<Location> getAll();

    List<Location> getInternalLocationsByLocation(Integer idLocation);
    
}