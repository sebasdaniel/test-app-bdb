package com.bdb.locations.repository;

import java.util.List;

import com.bdb.locations.model.Location;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LocationRepository extends JpaRepository<Location, Integer> {
    
    @Query("SELECT l FROM Location l WHERE parentLocation.id = :idParent")
    List<Location> getInternalLocationsByLocationId(@Param("idParent") Integer idLocation);
    
}