CREATE TABLE location(
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    areaM2 NUMERIC NOT NULL,
    parentLocation INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY (parentLocation) REFERENCES location(id)
);