package com.bdb.locations.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.bdb.locations.model.Location;
import com.bdb.locations.repository.LocationRepository;
import com.bdb.locations.service.contract.ILocationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LocationServiceTest {
    
    @Mock
    private LocationRepository repoLocation;

    @InjectMocks
    private ILocationService serviceLocation = new LocationServiceJpa();

    @BeforeEach
    void setMockOutput() {

        Location location = new Location();
        location.setName("Room");
        location.setAreaM2(25f);

        List<Location> allLocations = Arrays.asList(location);

        when(repoLocation.findAll()).thenReturn(allLocations);
    }

    @Test
    void testGetLocations_NotNull() {
        assertNotEquals(null, serviceLocation.getAll());
    }

    @Test
    void testGetLocations_NameIsOk() {
        assertEquals("Room", serviceLocation.getAll().get(0).getName());
    }

    @Test
    void testGetLocations_AreaM2IsOk() {
        assertEquals(25f, serviceLocation.getAll().get(0).getAreaM2());
    }

}